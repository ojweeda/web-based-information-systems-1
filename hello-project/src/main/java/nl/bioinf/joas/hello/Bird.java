package nl.bioinf.joas.hello;

public class Bird {
    private String speciesName;
    private String genusName;
    private String englishName;


    public Bird(String speciesName, String genusName, String englishName) {
        this.speciesName = speciesName;
        this.genusName = genusName;
        this.englishName = englishName;
    }

    public String getSpeciesName() {
        return englishName;
    }

    public String getGenusName() {
        return genusName;
    }

    public String getEnglishName() {
        return englishName;
    }
}
