package nl.bioinf.joas.heartrate.servlets;

import nl.bioinf.joas.heartrate.config.WebConfig;
import nl.bioinf.joas.heartrate.model.PhraseFactory;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.WebContext;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Locale;

@WebServlet(name = "PhraseServlet", urlPatterns = "/give.phrase")
public class PhraseServlet extends HttpServlet {
    private TemplateEngine templateEngine;

    @Override
    public void init() throws ServletException {
        final ServletContext servletContext = super.getServletContext();
        this.templateEngine = WebConfig.createTemplateEngine(servletContext);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String phraseType = request.getParameter("phrase_category");
        Locale locale = request.getLocale();
        WebContext ctx = new WebContext(
                request,
                response,
                request.getServletContext(),
                locale);
        if (phraseType != null) {
            final String phrase = PhraseFactory.getPhrase(phraseType);
            ctx.setVariable("phrase_type", phraseType);
            ctx.setVariable("phrase_num", phrase);
        } else {
            ctx.setVariable("phrase_type", "none");
            ctx.setVariable("phrase_num", "0");
        }
        templateEngine.process("phrase_of_the_day", ctx, response.getWriter());
    }
}