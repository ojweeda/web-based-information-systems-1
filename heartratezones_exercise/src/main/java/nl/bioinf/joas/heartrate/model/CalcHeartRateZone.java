package nl.bioinf.joas.heartrate.model;

public class CalcHeartRateZone {
    public static String getZoneIndex(int heartRate, int zone) throws IllegalArgumentException{
        if (zone == 1) {
            return String.format("%d - %d", (int) (heartRate * 0.5), (int) (heartRate * 0.6));
        } else if (zone == 2) {
            return String.format("%d - %d", (int) (heartRate * 0.6), (int) (heartRate * 0.7));
        } else if (zone == 3) {
            return String.format("%d - %d", (int) (heartRate * 0.7), (int) (heartRate * 0.8));
        } else if (zone == 4) {
            return String.format("%d - %d", (int) (heartRate * 0.8), (int) (heartRate * 0.9));
        } else if (zone == 5) {
            return String.format("%d - %d", (int) (heartRate * 0.9), heartRate);
        }
        throw new IllegalArgumentException("This is not an integer");
    }
}
