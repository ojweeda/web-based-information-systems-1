package nl.bioinf.joas.heartrate.servlets;

import nl.bioinf.joas.heartrate.config.WebConfig;
import nl.bioinf.joas.heartrate.model.CalcHeartRateZone;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.WebContext;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Date;

@WebServlet(name = "HelloServlet", urlPatterns = "/heart-rate-zones", loadOnStartup = 1)
public class HeartRateServlet extends HttpServlet {
    private TemplateEngine templateEngine;

    @Override
    public void init() throws ServletException{
        final ServletContext servletContext = super.getServletContext();
        this.templateEngine = WebConfig.createTemplateEngine(servletContext);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String heartRate = request.getParameter("heart_rate");
        WebContext ctx = new WebContext(
                request,
                response,
                request.getServletContext(),
                request.getLocale());
        String zone1 = CalcHeartRateZone.getZoneIndex(Integer.parseInt(heartRate), 1);
        String zone2 = CalcHeartRateZone.getZoneIndex(Integer.parseInt(heartRate), 2);
        String zone3 = CalcHeartRateZone.getZoneIndex(Integer.parseInt(heartRate), 3);
        String zone4 = CalcHeartRateZone.getZoneIndex(Integer.parseInt(heartRate), 4);
        String zone5 = CalcHeartRateZone.getZoneIndex(Integer.parseInt(heartRate), 5);

        ctx.setVariable("zone1", zone1);
        ctx.setVariable("zone2", zone2);
        ctx.setVariable("zone3", zone3);
        ctx.setVariable("zone4", zone4);
        ctx.setVariable("zone5", zone5);

        templateEngine.process("zone_results", ctx, response.getWriter());
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        WebContext ctx = new WebContext(
                request,
                response,
                request.getServletContext(),
                request.getLocale());
        templateEngine.process("heart_rate", ctx, response.getWriter());
    }
}