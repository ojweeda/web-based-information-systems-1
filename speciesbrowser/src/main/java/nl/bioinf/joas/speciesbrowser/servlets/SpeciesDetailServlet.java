package nl.bioinf.joas.speciesbrowser.servlets;

import nl.bioinf.joas.speciesbrowser.config.WebConfig;
import nl.bioinf.joas.speciesbrowser.models.HistoryManager;
import nl.bioinf.joas.speciesbrowser.models.Panda;
import nl.bioinf.joas.speciesbrowser.models.PandaCollection;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.WebContext;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@WebServlet(name = "SpeciesDetailServlet", urlPatterns = "/species.detail", loadOnStartup = 0)
public class SpeciesDetailServlet extends HttpServlet {
    private TemplateEngine templateEngine;
    private static Map<String, Panda> pandas_map = new HashMap<>();


    @Override
    public void init() throws ServletException {
        final ServletContext servletContext = super.getServletContext();
        this.templateEngine = WebConfig.createTemplateEngine(servletContext);
        pandas_map = PandaCollection.PANDAS_MAP;
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        WebContext ctx = new WebContext(
                request,
                response,
                request.getServletContext(),
                request.getLocale());
        templateEngine.process("species_detail", ctx, response.getWriter());
    }

    private static void setVariables(String key, WebContext ctx) {
        ctx.setVariable("scName", pandas_map.get(key).getScientificName());
        ctx.setVariable("enName", pandas_map.get(key).getEnglishName());
        ctx.setVariable("duName", pandas_map.get(key).getDutchName());
        ctx.setVariable("picture", pandas_map.get(key).getPictureFile());
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        WebContext ctx = new WebContext(
                request,
                response,
                request.getServletContext(),
                request.getLocale());
        String species = request.getParameter("species");
        setVariables(species, ctx);
        HttpSession session = request.getSession();
        HistoryManager historyManager;
        if (session.isNew() || session.getAttribute("history") == null) {
            historyManager = new HistoryManager();
            session.setAttribute("history", historyManager);
        } else {
            historyManager = (HistoryManager)session.getAttribute("history");
        }
        historyManager.addItem(species);
        templateEngine.process("species_detail", ctx, response.getWriter());
    }
}