package nl.bioinf.joas.speciesbrowser.database_utilities;
import nl.bioinf.joas.speciesbrowser.database_utilities.DbCredentials;
import nl.bioinf.joas.speciesbrowser.database_utilities.DbUser;

import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.SQLException;

class DbDemo{
    void useCredentials() throws IOException, NoSuchFieldException {
        DbUser dbUser = DbCredentials.getMySQLUser();
        String user = dbUser.getUserName();
        String passWrd = dbUser.getDatabasePassword();
        String host = dbUser.getHost();
        String dbName = dbUser.getDatabaseName();
    }
}