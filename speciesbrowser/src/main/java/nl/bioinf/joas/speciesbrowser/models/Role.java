package nl.bioinf.joas.speciesbrowser.models;

    public enum Role {
        GUEST,
        USER,
        ADMIN;
    }
