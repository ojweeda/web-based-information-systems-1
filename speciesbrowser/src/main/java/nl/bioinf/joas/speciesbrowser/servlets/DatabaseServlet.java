package nl.bioinf.joas.speciesbrowser.servlets;

import nl.bioinf.joas.speciesbrowser.config.WebConfig;
import nl.bioinf.joas.speciesbrowser.database_utilities.DatabaseException;
import nl.bioinf.joas.speciesbrowser.database_utilities.MyAppDao;
import nl.bioinf.joas.speciesbrowser.database_utilities.MyAppDaoFactory;
import nl.bioinf.joas.speciesbrowser.models.User;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.WebContext;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "DatabaseServlet", urlPatterns = "/user.details")
public class DatabaseServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        final MyAppDao dataSource = MyAppDaoFactory.getDataSource();
        try {
            final User user = dataSource.getUser("Joas", "JHZsTSJ2");
            final ServletContext servletContext = super.getServletContext();
            final TemplateEngine templateEngine = WebConfig.createTemplateEngine(servletContext);
            WebContext ctx = new WebContext(
                    request,
                    response,
                    request.getServletContext(),
                    request.getLocale());
            System.out.println(user.getName());
            System.out.println(user.getEmail());
            System.out.println(user.getPassword());
            ctx.setVariable("user", user);
            templateEngine.process("user-details", ctx, response.getWriter());

        } catch (DatabaseException e) {
            e.printStackTrace();
        }

    }
}
