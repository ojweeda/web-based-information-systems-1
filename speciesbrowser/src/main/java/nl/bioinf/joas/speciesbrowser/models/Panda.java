package nl.bioinf.joas.speciesbrowser.models;

public class Panda {
    private String scientificName;
    private String englishName;
    private String dutchName;
    private String pictureFile;


    public Panda(String scientificName, String englishName, String dutchName, String pictureFile) {
        this.scientificName = scientificName;
        this.englishName = englishName;
        this.dutchName = dutchName;
        this.pictureFile = pictureFile;
    }

    public String getScientificName() {
        return scientificName;
    }

    public String getEnglishName() {
        return englishName;
    }

    public String getDutchName() { return dutchName; }

    public String getPictureFile() {
        return pictureFile;
    }

    @Override
    public String toString() {
        return scientificName + ", " +  englishName + ", " + dutchName + ", " + pictureFile + ".\n";
    }
}
