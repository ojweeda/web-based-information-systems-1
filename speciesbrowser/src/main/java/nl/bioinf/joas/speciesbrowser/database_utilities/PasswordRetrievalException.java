package nl.bioinf.joas.speciesbrowser.database_utilities;

public class PasswordRetrievalException extends Exception {

    public PasswordRetrievalException(String arg0) {
        super(arg0);
    }
}