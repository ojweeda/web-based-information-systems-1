package nl.bioinf.joas.speciesbrowser.database_utilities;

import nl.bioinf.joas.speciesbrowser.database_utilities.DbCredentials;
import nl.bioinf.joas.speciesbrowser.database_utilities.DbUser;

import java.io.IOException;

public class MyAppDaoFactory {

    private static MyAppDao daoInstance;

    /**
     * Code should be called at application startup
     */
    public static void initializeDataSource(String type) throws DatabaseException, ClassNotFoundException {
        if (daoInstance != null) {
            throw new IllegalStateException("DAO can be initialized only once");
        }
        switch (type) {
            case "dummy": {
                createDummyInstance();
                break;
            }
            case "mysql": {
                createMySQLInstance();
                break;
            }
            default:
                throw new IllegalArgumentException("unknown database type requested");
        }
    }

    /**
     * serves the dao instance
     *
     * @return
     */
    public static MyAppDao getDataSource() {
        if (daoInstance == null) {
            throw new IllegalStateException("DAO is not initialized; call initializeDataSource() first");
        }
        return daoInstance;
    }

    private static void createDummyInstance() throws DatabaseException, ClassNotFoundException {
        daoInstance = new MyAppDaoInMemory();
        daoInstance.connect();
    }

    private static void createMySQLInstance() throws DatabaseException {
        try {
            DbUser mySQLuser = DbCredentials.getMySQLUser();
            String dbUrl = "jdbc:mysql://" + mySQLuser.getHost() + "/" + mySQLuser.getDatabaseName();
            String dbUser = mySQLuser.getUserName();
            String dbPass = mySQLuser.getDatabasePassword();
            daoInstance = MyAppDaoMySQL.getInstance(dbUrl, dbUser, dbPass);
            daoInstance.connect();
        } catch (IOException | NoSuchFieldException | ClassNotFoundException e) {
            throw new DatabaseException(e);
        }
    }
}

