package nl.bioinf.joas.speciesbrowser.models;

import java.io.*;
import java.util.*;

public class PandaCollection {
    public static List<Panda> PANDAS = new ArrayList<>();
    public static Map<String, Panda> PANDAS_MAP = new HashMap<>();

    static {
        PandaCollection.PANDAS = readTXTIntoList();
        PandaCollection.PANDAS_MAP = readTXTIntoMap();
    }

    public List<Panda> getPandasList() {
        return Collections.unmodifiableList(PANDAS);
    }

    public Map<String, Panda> getPandasMap() {
        return Collections.unmodifiableMap(PANDAS_MAP);
    }

    private static Panda createPanda(String[] attributes) {
        String sfName = attributes[0];
        String enName = attributes[1];
        String duName = attributes[2];
        String picture = attributes[3];
        return new Panda(sfName, enName, duName, picture);
    }

    private static List<Panda> readTXTIntoList() {
        String csvFile = "/homes/ojweeda/Desktop/Jaar3/Thema10/web-based-information-systems-1/speciesbrowser/data/species.txt";
        BufferedReader br = null;
        List<Panda> pandas = new ArrayList<>();
        String line;
        try {
            br = new BufferedReader(new FileReader(csvFile));
            while ((line = br.readLine()) != null) {
                if (!line.startsWith("scientific_name")) {
                    String[] attributes = line.split(";");
                    Panda panda = createPanda(attributes);
                    pandas.add(panda);
                }
            }
        } catch (IOException | NullPointerException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return pandas;
    }

    private static Map<String, Panda> readTXTIntoMap() {
        String csvFile = "/homes/ojweeda/Desktop/Jaar3/Thema10/web-based-information-systems-1/speciesbrowser/data/species.txt";
        BufferedReader br = null;
        Map<String, Panda> pandas = new HashMap<>();
        String line;
        try {
            br = new BufferedReader(new FileReader(csvFile));
            while ((line = br.readLine()) != null) {
                if (!line.startsWith("scientific_name")) {
                    String[] attributes = line.split(";");
                    pandas.put(attributes[1], createPanda(attributes));
                }
            }
        } catch (NullPointerException | IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return pandas;
    }
}
