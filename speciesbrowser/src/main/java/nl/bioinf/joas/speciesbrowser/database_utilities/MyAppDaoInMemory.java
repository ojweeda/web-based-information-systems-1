package nl.bioinf.joas.speciesbrowser.database_utilities;

import nl.bioinf.joas.speciesbrowser.models.Role;
import nl.bioinf.joas.speciesbrowser.models.User;

import java.util.HashMap;
import java.util.Map;

public class MyAppDaoInMemory implements MyAppDao {
    private Map<String, User> userDb = new HashMap<>();

    public MyAppDaoInMemory() {
        createDb();
    }

    @Override
    public void connect() throws DatabaseException {
        //pass silently
    }

    @Override
    public User getUser(String userName, String userPass) throws DatabaseException {
        if (this.userDb.containsKey(userName)) return this.userDb.get(userName);
        return null;
    }

    @Override
    public void insertUser(String userName, String userPass, String email, Role role) throws DatabaseException {
        this.userDb.put(userName, new User(userName, userPass, email, role));
    }

    @Override
    public void disconnect() throws DatabaseException {
        //pass silently
    }

    private void createDb() {
        userDb.put("Bas",
                new User("Bas", "bdoddema@st.hanze.nl", "1234567", Role.ADMIN));
        userDb.put("Joas",
                new User("Joas", "ojweeda@st.hanze.nl", "JHZsTSJ2", Role.USER));


    }

}
