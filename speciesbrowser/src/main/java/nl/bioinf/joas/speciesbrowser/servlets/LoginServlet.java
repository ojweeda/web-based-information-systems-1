package nl.bioinf.joas.speciesbrowser.servlets;

import nl.bioinf.joas.speciesbrowser.config.WebConfig;
import nl.bioinf.joas.speciesbrowser.models.Panda;
import nl.bioinf.joas.speciesbrowser.models.PandaCollection;
import nl.bioinf.joas.speciesbrowser.models.Role;
import nl.bioinf.joas.speciesbrowser.models.User;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.WebContext;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@WebServlet(urlPatterns = "/login", loadOnStartup = 0)
public class LoginServlet extends HttpServlet {
    private TemplateEngine templateEngine;
    private static List<Panda> pandas = new ArrayList<>();

    @Override
    public void init() throws ServletException {
        final ServletContext servletContext = super.getServletContext();
        this.templateEngine = WebConfig.createTemplateEngine(servletContext);

    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String username = request.getParameter("username");
        String password = request.getParameter("password");
        String nextPage;

        WebContext ctx = new WebContext(
                request,
                response,
                request.getServletContext(),
                request.getLocale());
        //fetch the session object
        //if it is not present, one will be created
        HttpSession session = request.getSession();
        pandas = PandaCollection.PANDAS;
        ctx.setVariable("pandas_list", pandas);
        ctx.setVariable("name_of_panda_selector", "Panda's");


        if (session.isNew() || session.getAttribute("user") == null) {
            boolean authenticated = authenticate(username, password);
            if (authenticated) {
                session.setAttribute("user", new User("Joas", "henk@example.com", Role.USER));
                nextPage = "listing";
            } else {
                ctx.setVariable("message", "Your password or username is incorrect; please try again");
                ctx.setVariable("message_type", "error");
                nextPage = "login";
            }
        } else {
            nextPage = "listing";
        }
        templateEngine.process(nextPage, ctx, response.getWriter());
    }

    private boolean authenticate(String username, String password) {
        return username.equals("Joas") && password.equals("bas");
    }

    //simple GET requests are immediately forwarded to the login page
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        WebContext ctx = new WebContext(
                request,
                response,
                request.getServletContext(),
                request.getLocale());
        ctx.setVariable("message", "Fill out the login form");
        ctx.setVariable("message_type", "info");
        templateEngine.process("login", ctx, response.getWriter());
    }
}
