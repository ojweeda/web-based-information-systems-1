package nl.bioinf.joas.speciesbrowser.servlets;

import nl.bioinf.joas.speciesbrowser.config.WebConfig;
import nl.bioinf.joas.speciesbrowser.models.HistoryManager;
import nl.bioinf.joas.speciesbrowser.models.PandaCollection;
import nl.bioinf.joas.speciesbrowser.models.Panda;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.WebContext;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@WebServlet(name = "HomeServlet", urlPatterns = "/home", loadOnStartup = 0)
public class HomeServlet extends HttpServlet {
    private TemplateEngine templateEngine;
    private static List<Panda> pandas = new ArrayList<>();

    @Override
    public void init() throws ServletException {
        final ServletContext servletContext = super.getServletContext();
        this.templateEngine = WebConfig.createTemplateEngine(servletContext);
        pandas = PandaCollection.PANDAS;
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        WebContext ctx = new WebContext(
                request,
                response,
                request.getServletContext(),
                request.getLocale());
        templateEngine.process("listing", ctx, response.getWriter());
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        WebContext ctx = new WebContext(
                request,
                response,
                request.getServletContext(),
                request.getLocale());
        HttpSession session = request.getSession();
        HistoryManager historyManager;
        if (session.isNew()) {
            historyManager = new HistoryManager(5);
            session.setAttribute("history", historyManager);
        }
//        System.out.println("history = " + request.getSession().getAttribute("history").toString());
        ctx.setVariable("pandas_list", pandas);
        request.getSession().getAttribute("pandas_list");
        ctx.setVariable("banner", "banner_group_of_animals.jpg");
        templateEngine.process("listing", ctx, response.getWriter());
    }
}
